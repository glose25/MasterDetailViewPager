package com.jayglose.masterdetailviewpager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class GenericMasterFragment extends Fragment //{
    implements GenericListFragment.Callbacks {

    private boolean mTwoPane;
    FragmentActivity activity;
    View frameLayout;
    private String type;

    public GenericMasterFragment(String type) {
        this.type = type;
    }

    public GenericMasterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        //getChildFragmentManager()
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        frameLayout = inflater.inflate(R.layout.fragment_generic_master, container, false);

        Fragment listFragment = new GenericListFragment(type);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.frame_generic_main, listFragment);

        if (frameLayout.findViewById(R.id.frame_generic_extra) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            Fragment detailFragment = new GenericDetailFragment();
            transaction.add(R.id.frame_generic_extra, detailFragment);

        }

        transaction.commit();

        return frameLayout;
    }

    @Override
    public void onItemSelected(String id) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(GenericDetailFragment.ARG_ITEM_ID, id);

            arguments.putString(GenericDetailFragment.CONTENT_TYPE, type);

            GenericDetailFragment fragment = new GenericDetailFragment();

            fragment.setArguments(arguments);
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_generic_extra, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(activity, GenericDetailActivity.class);
            detailIntent.putExtra(GenericDetailFragment.ARG_ITEM_ID, id);
            detailIntent.putExtra(GenericDetailFragment.CONTENT_TYPE, type);
            startActivity(detailIntent);
        }
    }

}
