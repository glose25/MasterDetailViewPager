package com.jayglose.masterdetailviewpager;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jayglose.masterdetailviewpager.data.FakeBookContent;
import com.jayglose.masterdetailviewpager.data.FakeItemsContent;

public class GenericDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";
    public static final String CONTENT_TYPE = "content_type";

    private FakeBookContent.DummyItem fakeBook;
    private FakeItemsContent.DummyItem fakeItem;

    public GenericDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (getArguments().containsKey(ARG_ITEM_ID)) {

                String contentType = getArguments().getString(CONTENT_TYPE);
                String title = "";

                fakeBook = null;
                fakeItem = null;

                if (contentType != null) {
                    if (contentType.equalsIgnoreCase("item")) {
                        fakeItem = FakeItemsContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
                        title = fakeItem.content;
                    }
                    if (contentType.equalsIgnoreCase("book")) {
                        fakeBook = FakeBookContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
                        title = fakeBook.content;
                    }

                    Activity activity = this.getActivity();
                    CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
                    if (appBarLayout != null) {
                        appBarLayout.setTitle(title);
                    }
                }

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_generic_detail, container, false);

        String details = "";
        if (fakeBook != null) {
            details = fakeBook.details;
        }
        if (fakeItem != null) {
            details = fakeItem.details;
        }

        ((TextView) rootView.findViewById(R.id.generic_detail)).setText(details);

        return rootView;
    }
}
