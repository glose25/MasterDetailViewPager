I'm struggling with Master/Detail in a ViewPager on a tablet, ONLY when I need to re-use fragments and layouts for generic content.

The PEOPLE page uses it's own fragments and layouts, and it works fine.

But the BOOKS and ITEMS pages (tabs) use the same fragments and layouts. When I implement this with TabHost, it works fine. But because ViewPager loads a page before and after the active page, the detail fragment content is either displaying on the wrong page, or not at all (depending on which tab you select first).